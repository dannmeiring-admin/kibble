import 'package:flutter/cupertino.dart';

class KibbleAnimator {
  Widget animatedSwitcher({required Widget child}) {
    return AnimatedSwitcher(
      switchInCurve: Curves.linearToEaseOut,
      switchOutCurve: Curves.linearToEaseOut,
      transitionBuilder: (Widget child, Animation<double> animation) =>
          FadeTransition(
        opacity: animation,
        child: ScaleTransition(
          child: child,
          scale: animation,
        ),
      ),
      duration: const Duration(milliseconds: 500),
      child: child,
    );
  }
}
